let btnSnakeColor = document.querySelectorAll('.snakeColor input');
let btnMapColor = document.querySelectorAll('.mapColor input');
let btnGameSpeed = document.querySelectorAll('.gameSpeed input');
let btnFieldSize = document.querySelectorAll('.fieldSize input');

let X;

function switchOption(btn) {

    for (let i = 0; i < btn.length; i++) {

        option = btn[i].addEventListener('click', function (e) {

            for (let i = 0; i < btn.length; i++) {
                if (btn[i].disabled == true) {
                    btn[i].disabled = false;
                }
            }

            let parrent = e.target.parentElement.parentElement.className;

            if (parrent == "snakeColor") {
                snake.color = e.target.name;
                map();
                snake.drawSnake();

            } else if (parrent == "mapColor") {
                mapColor = e.target.name;
                map();
                snake.drawSnake();

            } else if (parrent == "gameSpeed") {
                
                clearInterval(repeater);
                gameSpeed = e.target.name;
                repeater = setInterval(game, gameSpeed);

            } else if (parrent == "fieldSize") {
                console.log("fieldSize");
                fieldSize = e.target.name;
                snake.height = fieldSize - 2;
                snake.width = fieldSize - 2;
                
                apple.height = fieldSize - 2;
                apple.width = fieldSize - 2;
                
                apple.random();
            }

            e.target.disabled = true;
        });
    }
}

switchOption(btnSnakeColor);
switchOption(btnMapColor);
switchOption(btnGameSpeed);
switchOption(btnFieldSize);

let game_start = false;
let body = document.querySelector('body');


body.addEventListener('keydown', function (e) {
    
    
    if (e.keyCode == " ".charCodeAt(0)) {
        

        if (game_start && !snake.isDead) {
            
            ctx.fillStyle = "white";
            ctx.fillRect(csize/4, csize/3,csize/2, csize/4);
            ctx.font = "60px serif";
            ctx.fillStyle = "Black";
            ctx.fillText("PAUZA", 150, 250);
        }
        game_start = !game_start;
    }
})

let btnReset = document.querySelector('.game .tryAgain');
btnReset.addEventListener('click', snake.reset);

let btnStart = document.getElementById('start');
btnStart.addEventListener('click', function(){
    let menu = document.querySelector('.menu');
    menu.style.display = "none";
    game_start = true;
})