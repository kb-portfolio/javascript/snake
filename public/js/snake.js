const canvas = document.querySelector('#snake');
const ctx = canvas.getContext('2d');

let csize = 500;
let mapColor = '#000';
let strokeColor = '#444';
let fieldSize = 10;
let snakeColor = '#00ff00';
let gameSpeed = 75;
let appleColor = '#ff0000';
let colisionOn = true;


canvas.width = csize;
canvas.height = csize;

let snake = {
    length: 5,
    snakeParts: [],
    color: snakeColor,
    height: fieldSize - 2,
    width: fieldSize - 2,
    posX: 2,
    posY: 2,
    speedX: 1,
    speedY: 0,
    direction: "D",
    lastMove: "D",
    score: 0,
    isDead: false
};

let apple = {
    posX: -1,
    posY: -1,
    height: fieldSize - 2,
    width: fieldSize - 2,
    color: appleColor,
    eaten: false,
    tasty: 5
}

snake.partsManagement = function () {

    if (this.length > 1) {
        this.snakeParts[0] = [snake.posX, snake.posY];
    }

    for (let i = this.length - 1; i > 0; i--) {

        if (this.snakeParts[i - 1] != undefined) {
            this.snakeParts[i] = this.snakeParts[i - 1]
        }
    }

};

snake.drawSnake = function () {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.posX * fieldSize + 1, this.posY * fieldSize + 1, this.height, this.width);

    for (let i = this.length - 1; i > 0; i--) {
        if (this.snakeParts[i] != undefined) {
            ctx.fillRect(this.snakeParts[i][0] * fieldSize + 1, this.snakeParts[i][1] * fieldSize + 1, this.height, this.width);
        }
    }
};

snake.movment = function () {

    switch (this.direction) {
        case "W":
            this.posY--;
            this.lastMove = "W";
            break;

        case "S":
            this.posY++;
            this.lastMove = "S";
            break;

        case "D":
            this.posX++;
            this.lastMove = "D";
            break;

        case "A":
            this.posX--;
            this.lastMove = "A";
            break;

    }

    if (this.posX > csize / fieldSize - 1) {
        this.posX = 0;
    } else if (this.posX < 0) {
        this.posX = csize / fieldSize - 1;
    }


    if (this.posY > csize / fieldSize - 1) {
        this.posY = 0;
    } else if (this.posY < 0) {
        this.posY = csize / fieldSize - 1;
    }


}



snake.changeDirection = function () {
    let body = document.querySelector('body');
    let that = this;

    document.onkeydown = function (e) {

        switch (e.keyCode) {
            case "W".charCodeAt(0):
                if (that.lastMove != "S") {
                    that.direction = "W";
                }
                break;

            case "S".charCodeAt(0):
                if (that.lastMove != "W") {
                    that.direction = "S";
                }
                break;

            case "D".charCodeAt(0):
                if (that.lastMove != "A") {
                    that.direction = "D";
                }
                break;

            case "A".charCodeAt(0):
                if (that.lastMove != "D") {
                    that.direction = "A";
                }
                break;
        }
    };

}

snake.eat = function () {
    if (this.posX == apple.posX && this.posY == apple.posY) {
        this.length += apple.tasty;
        this.score += (apple.tasty * 10);
        apple.random();
        updateScore();
    }
}


snake.checkCollision = function () {

    if (colisionOn) {
        for (let i = 0; i < this.length; i++) {
            if (this.snakeParts[i] != undefined) {
                if (this.posX == this.snakeParts[i][0] && this.posY == this.snakeParts[i][1]) {
                    this.isDead = true;
                    snake.theEnd();
                }
            }
        }
    }

}

snake.theEnd = function () {

    let overlay = document.querySelector('.overlay');
    overlay.style.display = "block";
    let score = overlay.querySelector('.score');
    score.innerHTML = "Score: " + this.score;
    game_start = false;

}

snake.reset = function () {
    snake.length = 5;
    snake.snakeParts = [];
    snake.posX = 2;
    snake.posY = 2;
    snake.speedX = 1;
    snake.speedY = 0;
    snake.direction = "D";
    snake.lastMove = "D";
    snake.score = 0;
    snake.isDead = false;

    let overlay = document.querySelector('.overlay');
    overlay.style.display = "none";

    updateScore();
    let menu = document.querySelector('.menu');
    menu.style.display = "block";
    map();

}

apple.random = function () {
    
    this.posX = Math.floor(Math.random() * (csize / fieldSize));
    this.posY = Math.floor(Math.random() * (csize / fieldSize));

    let inBody = true;
    while (inBody) {

        this.posX = Math.floor(Math.random() * (csize / fieldSize));
        this.posY = Math.floor(Math.random() * (csize / fieldSize));
        if (this.posX == snake.posX&&
            this.posY == snake.posY)  inBody = true;
                    

        if (snake.snakeParts[0] != undefined) {
            for (let i = 1; i < snake.snakeParts.length; i++) {
                if (this.posX == snake.snakeParts[i][0] &&
                    this.posY == snake.snakeParts[i][1]) {
                    
                    inBody = true;
                    break;
                }
                inBody = false;
            }
        } else {
            inBody = false;
        }
        
    }

};

apple.draw = function () {
    if (this.posX > -1) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.posX * fieldSize + 1, this.posY * fieldSize + 1, this.height, this.width);
    }
}



function map() {

    ctx.lineWidth = 2;
    ctx.fillStyle = mapColor;
    ctx.strokeStyle = strokeColor;

    ctx.fillRect(0, 0, csize, csize);

    for (let i = 0; i < csize / fieldSize; i++) {
        for (let j = 0; j < csize / fieldSize; j++) {
            ctx.fillRect(1 + (i * fieldSize), 1 + (j * fieldSize), fieldSize, fieldSize);
            ctx.strokeRect((i * fieldSize), (j * fieldSize), fieldSize, fieldSize);
        }
    }
}

function updateScore() {
    let score = document.querySelector('.score').innerHTML = "Score: " + snake.score;
}

map();
snake.drawSnake();
apple.random();


function game() {
    if (game_start && !snake.isDead) {
        map();
        snake.changeDirection();
        snake.partsManagement();
        snake.movment();

        apple.draw();
        snake.drawSnake();
        snake.eat();
        snake.checkCollision();

    }
}

let repeater = setInterval(game, gameSpeed);

//let body = document.querySelector('body');
//body.addEventListener('click', game);
